import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';
import { FileService } from 'src/app/services/file.service';

@Component({
  selector: 'app-upload2',
  templateUrl: './upload2.component.html',
  styleUrls: ['./upload2.component.css']
})
export class Upload2Component implements OnInit {

  file: any;

  constructor(private msg: NzMessageService, private fileService: FileService) { }

  ngOnInit() {
  }

  handleChange(info: NzUploadChangeParam): void {
    //on passe à notre global variable
    this.file = info.file;
    //on teste
    if (info.file.status !== 'uploading') {
      console.log("1");
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      console.log("2");
      this.msg.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      console.log("3");
      this.msg.error(`${info.file.name} file upload failed.`);
    }
  }

  async sendFile(): Promise<void> {
    console.log(this.file);
    var splitTab = this.file.name.split(".");
    const currentFileExtension = splitTab[splitTab.length - 1];
    //conversion en blob

    const fileBlob = new Blob([this.file], { type: "image/"+currentFileExtension });
    //const fileBlob = new Blob([this.file]);
    console.log(fileBlob);
    //il faut un formData pour le faire
    let formData: FormData = new FormData();
    //formData.append('avatar', this.file, this.file.name);
    formData.append('avatar', fileBlob, this.file.name);

    console.log(formData)

    this.sendInAPI(formData); 

  }

  //envoyer vers l'api
  sendInAPI(obj: any) {
    //let les = {"user":JSON.stringify(obj)};

    console.log(obj);
    this.fileService.sendFile(obj).subscribe(
      (reponse) => {
        if (reponse.status === true) {
          //toute le suite : redirection
          alert('Fichier chargé')
          console.log("lien du fichier")
          console.log(reponse.link)

        }

        else {
          alert('Fichier non chargé')
        }
      },
      (error) => {
        alert('probleme de connexion ');
        console.log(error);
      },
    );
  }

}
