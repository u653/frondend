import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Upload1Component } from './components/upload1/upload1.component';
import { Upload2Component } from './components/upload2/upload2.component';

const routes: Routes = [
  {path:"",redirectTo:'upload1',pathMatch:'full'},
  {path:"upload1",component:Upload1Component},
  {path:"upload2",component:Upload2Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
