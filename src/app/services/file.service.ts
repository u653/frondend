import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http:HttpClient) { }

  

  sendFile(jsonUser:any): Observable  <any> {
    //requete post a un parametre en plus de l'url
    return this.http.post<any>('http://localhost:3000/upload-avatar-blob',jsonUser);
  }
}
